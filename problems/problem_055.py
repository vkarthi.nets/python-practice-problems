# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VII"
#     * input: 9
#       returns: "IX"
#     * input: 1
#       returns:  "X"

# def simple_roman(num):
#     roman = ""
#     if num >= 1 and num <= 3:
#         while len(roman) < num:
#             roman += "I"
#     elif num == 4 or num == 9:
#         roman += "I"
#         if num > 4:
#             roman += "X"
#         else:
#             roman += "V"
#     elif num >= 5 and num <= 8:
#         roman += "V"
#         if num >= 6:
#             roman += "I"
#         if num >= 7:
#             roman += "I"
#     elif num == 10:
#         roman = "X"
#     return roman

def simple_roman(num):
    if num == 1:
        return "I"
    elif num == 2:
        return "II"
    elif num == 3:
        return "III"
    elif num == 4:
        return "IV"
    elif num == 5:
        return "V"
    elif num == 6:
        return "VI"
    elif num == 7:
        return "VII"
    elif num == 8:
        return "VII"
    elif num == 9:
        return "IX"
    elif num == 10:
        return "X"
